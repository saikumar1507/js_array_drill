let items = require('./items.js')

function map(elements, cb) {
    var Multiple_of_3_array = []
    for (let index = 0; index < elements.length; index++) {
        Multiple_of_3_array.push(cb(elements[index]))
    }
    return Multiple_of_3_array
}
module.exports = map
