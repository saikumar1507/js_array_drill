function flattenArray(arr) {
    let newArray = []
    function recursive(arr) {
        for (let i = 0; i < arr.length; i++) {
            if (Array.isArray(arr[i])) {
                recursive(arr[i])
            } else {
                newArray.push(arr[i])
            }
        }
    }
    recursive(arr)
    return newArray
}
module.exports = flattenArray










// function flattenArray(arr){
//     let newArray=[]
//     for(let i=0;i<arr.length;i++){
//         if(Array.isArray(arr[i])){
//             let flat = flattenArray(arr[i])
//             newArray = newArray.concat(flat)
//         }else{
//             newArray.push(arr[i])
//         }
//     }
//     return newArray

// }
// console.log(flattenArray(l))

