function find(elements, cb) {
    let Result = 0
    for (let element of elements) {
        let value = cb(element)
        Result = value
    }
    return Result
}

module.exports = find