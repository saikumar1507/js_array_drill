const items = require('./items.js')

function reduce(elements, cb, startingValue) {
    let total;
    if (startingValue === undefined) {
        total = elements[0]
    } else {
        total = startingValue
    }

    let startIndex;
    if (startingValue === undefined) {
        startIndex = 1
    } else {
        startIndex = 0
    }
    for (let index = startIndex; index < elements.length; index++) {
        total = cb(total, elements[index])
    }
    return total
}
module.exports = reduce