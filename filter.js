let elements = [1, 2, 3, 4, 5, 6, 7]

let filtered_element = []

function filter(elements, cb) {
    for (let element of elements) {
        var value = cb(element)
        if (value != undefined) {
            filtered_element.push(value)
        }
    }
    return filtered_element
}

module.exports = filter
