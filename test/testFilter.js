const items = require('../items.js')

const filter = require('../filter.js')


function cb(element) {
    if (element % 2 == 0) {
        return element
    }
}
const Actual_output = filter(items, cb)


const Expected_output = [2, 4]

if (JSON.stringify(Actual_output) == JSON.stringify(Expected_output)) {
    console.log('Even numbers are', Actual_output)
} else {
    console.log('Result does not matched')
}
