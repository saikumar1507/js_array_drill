var nestedArray = [1, [2], [[3]], [[[4]]]]

const flattenArray = require('../flatten.js')

const actualArray = flattenArray(nestedArray)

const expectedArray = [1, 2, 3, 4]

if (JSON.stringify(actualArray) == JSON.stringify(expectedArray)) {
    console.log('Flatten array is ', actualArray)
} else {
    console.log('')
}