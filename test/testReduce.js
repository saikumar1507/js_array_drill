const items = require('../items.js')

const reduce = require('../reduce.js')

function cb(startingValue, arrValue) {
    return startingValue + arrValue
}

let Actual_output = reduce(items, cb)

const Expected_output = 20

if (Actual_output == Expected_output) {
    console.log("The sum of array is : ",Actual_output)
} else {
    console.log("Does't match")
}