const items = require('../items.js')

const map = require('../map.js')

const Actual_output = map(items,cb)

const Expected_output = [ 3, 6, 9, 12, 15, 15 ]

function cb(element){
    return element * 3
}

if(JSON.stringify(Actual_output) == JSON.stringify(Expected_output)){
    console.log('Multiplied each element in an array by 3 : ',Actual_output)
}else{
    console.log('Wrong answer')
}